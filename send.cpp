#include <iostream>
#include "ElasticFrameProtocol.h"
#include "RISTNet.h"

#define MTU 1440 //RIST-max
#define WORKER_VIDEO_FRAMES 3000

RISTNetSender myRISTNetSender; //The RIST sender
ElasticFrameProtocolSender myEFPSender(MTU); //EFP sender

//This will act as our encoder and just provide us with a H264 AnnexB frame when we want one.
std::vector<uint8_t> getNALUnit(int i) {
  std::string fileName = "../media/" + std::to_string(i) + ".h264";
  FILE *f = fopen(fileName.c_str(), "rb");
  if (!f) {
    std::cout << "Failed opening file" << std::endl;
    std::vector<uint8_t> empty;
    return empty;
  }
  fseek(f, 0, SEEK_END);
  size_t fSize = ftell(f);
  fseek(f, 0, SEEK_SET);
  uint8_t *pictureBuffer;
  int result = posix_memalign((void **) &pictureBuffer, 32, fSize);
  if (result) {
    std::cout << "Failed reserving memory" << std::endl;
    std::vector<uint8_t> empty;
    return empty;
  }
  size_t fResult = fread(pictureBuffer, 1, fSize, f);
  if (fResult != fSize) {
    std::cout << "Failed reading data" << std::endl;
    std::vector<uint8_t> empty;
    return empty;
  }
  std::vector<uint8_t> my_vector(&pictureBuffer[0], &pictureBuffer[fSize]);
  free(pictureBuffer);
  fclose(f);
  return my_vector;
}

//Just for debug
int packSize;

void sendData(const std::vector<uint8_t> &subPacket) {

  //for debugging actual bytes sent (payload bytes)
  if (subPacket[0] == 1 || subPacket[0] == 3) {
    packSize += subPacket.size() - myEFPSender.geType1Size();
  }

  //for debugging actual bytes sent (payload bytes)
  if (subPacket[0] == 2) {
    packSize += subPacket.size() - myEFPSender.geType2Size();
    std::cout << "Sent-> " << packSize << std::endl;
    packSize = 0;
  }

  bool result = myRISTNetSender.sendData(subPacket.data(),subPacket.size());
  if (!result) {
    std::cout << "Failed sending. Deal with that." << std::endl;
  }
}

int main() {
  packSize = 0;

  //Set-up framing protocol
  myEFPSender.sendCallback = std::bind(&sendData, std::placeholders::_1);

  //Set-up RIST

  //List of ip(name)/ports, weight of the interface and listen(true) or send mode
  std::vector<std::tuple<std::string, std::string, uint32_t, bool>> interfaceList;
  interfaceList.push_back(std::tuple<std::string, std::string, uint32_t, bool>("127.0.0.1", "8000", 5, false));

  RISTNetSender::RISTNetSenderSettings mySendConfiguration;
  mySendConfiguration.mPeerConfig.recovery_mode = RIST_RECOVERY_MODE_TIME;
  mySendConfiguration.mPeerConfig.recovery_maxbitrate = 100000;
  mySendConfiguration.mPeerConfig.recovery_maxbitrate_return = 0;
  mySendConfiguration.mPeerConfig.recovery_length_min = 1000;
  mySendConfiguration.mPeerConfig.recovery_length_max = 1000;
  mySendConfiguration.mPeerConfig.recover_reorder_buffer = 25;
  mySendConfiguration.mPeerConfig.recovery_rtt_min = 50;
  mySendConfiguration.mPeerConfig.recovery_rtt_max = 500;
  mySendConfiguration.mPeerConfig.bufferbloat_mode = RIST_BUFFER_BLOAT_MODE_OFF;
  mySendConfiguration.mPeerConfig.bufferbloat_limit = 6;
  mySendConfiguration.mPeerConfig.bufferbloat_hard_limit = 20;

  mySendConfiguration.mLogLevel = RIST_LOG_WARN;
  //mySendConfiguration.mPSK = "fdijfdoijfsopsmcfjiosdmcjfiompcsjofi33849384983943"; //Enable encryption by providing a PSK
  myRISTNetSender.initSender(interfaceList, mySendConfiguration);

  //Generate some data to send
  uint64_t pts = 0;
  uint64_t dts = 0;
  for (int i = 0; i < WORKER_VIDEO_FRAMES; ++i) {
    std::vector<uint8_t> thisNalData = getNALUnit(i + 1);
    std::cout << "SendNAL > " << thisNalData.size() << " pts " << pts << std::endl;
    //We got no DTS in this example set to PTS if no DTS is available (In this version of EFP DTS MUST be set to PTS if not available)
    dts = pts;
    myEFPSender.packAndSend(thisNalData,
                            ElasticFrameContent::h264,
                            pts,
                            dts,
                            EFP_CODE('A', 'N', 'X', 'B'),
                            1,
                            NO_FLAGS);
    //you could also send other data from other threads for example audio when the audio encoder spits out something.
    //myEFPSender.packAndSend(thisADTSData,ElasticFrameContent::adts,pts,'ADTS',1,NO_FLAGS);
    pts += 90000 / 60; //fake a pts of 60Hz. FYI.. the codestream is 23.98 (I and P only)
    usleep(1000 * 16); //sleep for 16ms ~60Hz
  }
  myRISTNetSender.destroySender();
  std::cout << "Done sending will exit" << std::endl;
  return 0;
}