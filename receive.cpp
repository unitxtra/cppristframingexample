#include <iostream>
#include "ElasticFrameProtocol.h"
#include "RISTNet.h"

#define MTU 1440 //RIST-max

//Create the receiver
RISTNetReceiver myRISTNetReceiver;

void gotData(ElasticFrameProtocolReceiver::pFramePtr &rPacket);

//**********************************
//Server part
//**********************************

// This is the class and everything you want to associate with a RIST connection
// You can see this as a classic c-'void* context' on steroids since RISTNet will own it and handle
// it's lifecycle. The destructor is called when the RIST connection is terminated. Magic!

class MyClass {
public:
  MyClass() {
    myEFPReceiver = new (std::nothrow) ElasticFrameProtocolReceiver(5, 2);
  }
  virtual ~MyClass() {
    *efpActiveElement = false; //Release active marker
    delete myEFPReceiver;
  };
  uint8_t efpId = 0;
  std::atomic_bool *efpActiveElement;
  ElasticFrameProtocolReceiver *myEFPReceiver;
};

// Array of 256 possible EFP receivers, could be millions but I just decided 256 change to your needs.
// You could make it much simpler just giving a new connection a uint64_t number++
std::atomic_bool efpActiveList[UINT8_MAX] = {false};
uint8_t getEFPId() {
  for (int i = 1; i < UINT8_MAX - 1; i++) {
    if (!efpActiveList[i]) {
      efpActiveList[i] = true; //Set active
      return i;
    }
  }
  return UINT8_MAX;
}

//Return a connection object. (Return nullptr if you don't want to connect to that client)
std::shared_ptr<NetworkConnection> validateConnection(std::string ipAddress, uint16_t port) {
  std::cout << "Connecting IP: " << ipAddress << ":" << unsigned(port) << std::endl;

  //Get EFP ID.
  uint8_t efpId = getEFPId();
  if (efpId == UINT8_MAX) {
    std::cout << "Unable to accept more EFP connections " << std::endl;
    return nullptr;
  }

  // Here we can put whatever into the connection. The object we embed is maintained by RISTNet
  // In this case we put MyClass in containing the EFP ID we got from getEFPId() and a EFP-receiver
  auto a1 = std::make_shared<NetworkConnection>(); // Create a connection
  a1->mObject = std::make_shared<MyClass>(); // And my object containing my stuff
  auto v = std::any_cast<std::shared_ptr<MyClass> &>(a1->mObject); //Then get a pointer to my stuff
  v->efpId = efpId; // Populate it with the efpId
  v->efpActiveElement =
      &efpActiveList[efpId]; // And a pointer to the list so that we invalidate the id when RIST drops the connection
  v->myEFPReceiver->receiveCallback =
      std::bind(&gotData, std::placeholders::_1); //In this example we aggregate all callbacks..
  return a1; // Now hand over the ownership to RISTNet
}

//Network data recieved callback.
void dataFromSender(const uint8_t *buf, size_t len, std::shared_ptr<NetworkConnection> &rConnection, struct rist_peer *pPeer) {
  //We got data from RISTNet
  auto v = std::any_cast<std::shared_ptr<MyClass> &>(rConnection->mObject); //Get my object I gave RISTNet
  v->myEFPReceiver->receiveFragmentFromPtr(buf,len,v->efpId); //unpack the fragment I got using the efpId created at connection time.
}

//ElasticFrameProtocol got som data from some efpSource.. Everything you need to know is in the rPacket
//meaning EFP stream number EFP id and content type. if it's broken the PTS value
//code with additional information of payload variant and if there is embedded data to extract and so on.
void gotData(ElasticFrameProtocolReceiver::pFramePtr &rPacket) {
  std::cout << "BAM... Got some NAL-units of size " << unsigned(rPacket->mFrameSize) <<
            " pts " << unsigned(rPacket->mPts) <<
            " is broken? " << rPacket->mBroken <<
            " from EFP connection " << unsigned(rPacket->mSource) <<
            std::endl;
}

int main() {

  //validate the connecting client
  myRISTNetReceiver.validateConnectionCallback =
      std::bind(&validateConnection, std::placeholders::_1, std::placeholders::_2);
  //receive data from the client
  myRISTNetReceiver.networkDataCallback =
      std::bind(&dataFromSender, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);

  //List of ip(name)/ports and listen(true) or send mode
  std::vector<std::tuple<std::string, std::string, bool>> interfaceList;
  interfaceList.push_back(std::tuple<std::string, std::string, bool>("0.0.0.0", "8000", true));

  RISTNetReceiver::RISTNetReceiverSettings myReceiveConfiguration;
  myReceiveConfiguration.mPeerConfig.recovery_mode = RIST_RECOVERY_MODE_TIME;
  myReceiveConfiguration.mPeerConfig.recovery_maxbitrate = 100000;
  myReceiveConfiguration.mPeerConfig.recovery_maxbitrate_return = 0;
  myReceiveConfiguration.mPeerConfig.recovery_length_min = 1000;
  myReceiveConfiguration.mPeerConfig.recovery_length_max = 1000;
  myReceiveConfiguration.mPeerConfig.recover_reorder_buffer = 25;
  myReceiveConfiguration.mPeerConfig.recovery_rtt_min = 50;
  myReceiveConfiguration.mPeerConfig.recovery_rtt_max = 500;
  myReceiveConfiguration.mPeerConfig.weight = 5;
  myReceiveConfiguration.mPeerConfig.bufferbloat_mode = RIST_BUFFER_BLOAT_MODE_OFF;
  myReceiveConfiguration.mPeerConfig.bufferbloat_limit = 6;
  myReceiveConfiguration.mPeerConfig.bufferbloat_hard_limit = 20;

  myReceiveConfiguration.mLogLevel = RIST_LOG_WARN;
  //myReceiveConfiguration.mPSK = "fdijfdoijfsopsmcfjiosdmcjfiompcsjofi33849384983943"; //Enable encryption by providing a PSK

  //Initialize the receiver
  if (!myRISTNetReceiver.initReceiver(interfaceList, myReceiveConfiguration)) {
    std::cout << "Failed starting the server" << std::endl;
    return EXIT_FAILURE;
  }


  //Run this server until ........
  while (true) {
    sleep(1);
  }

  //When you decide to quit garbage collect and stop threads....
  myRISTNetReceiver.destroyReceiver();

  std::cout << "Done serving. Will exit." << std::endl;
  return 0;
}

