![efprist logo](efprist.jpg)



# RIST + EFP Client/Server example


This example uses EFP ([ElasticFramingProtocol](https://bitbucket.org/unitxtra/efp/src/master/)) as a layer between the producer/consumer of data and the transport layer. As network protocol/transport layer RIST ([Reliable Internet Stream Transport](https://code.videolan.org/rist/librist)) is used.

a RIST C++ wrapper used by this example. The wrapper is located [here](https://code.videolan.org/rist/rist-cpp).


## Building

Requires cmake version >= **3.10** and **C++17**

**Release:**

```sh
cmake -DCMAKE_BUILD_TYPE=Release .
make
```

***Debug:***

```sh
cmake -DCMAKE_BUILD_TYPE=Debug .
make
```

***Output:*** 

```
send_efp_rist
receive_efp_rist
```

## Usage

First in the build directory ->

```
mkdir tmp
mv send_efp_rist tmp/
```

To run ->

```
1. Start the server 
./receive_efp_rist
2. Then start another terminal and ->
cd tmp
./send_efp_rist
```


## License

*MIT*

Read *LICENCE.md* for details